all: joiner

joiner: joiner.c
	gcc -o joiner -static --static joiner.c -lrt -lpthread

clean:
	rm -f joiner

.PHONY: clean
